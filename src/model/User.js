import { mongoose } from "mongoose";
import bcrypt from "bcrypt";

const UserSchema = mongoose.Schema(
  {
    token_temp: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    name: {
      type: String,
    }
  },
  { timestamps: true }
);

UserSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.statics.comparePassword = async (password, receivedPassword) => {
  return await bcrypt.compare(password, receivedPassword);
};

export default mongoose.model("Usuario", UserSchema);
