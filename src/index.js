import app from "./app";
import { Server as websocketServer } from "socket.io";
import http  from "http"
import sockets from "./sockets";
import { PORT } from "./config";

import { connectDB } from "./db";
connectDB();

const server =  http.createServer(app)  
const httpServer = server.listen(PORT)
console.log('Server on port', PORT)

const io = new websocketServer(httpServer)
sockets(io)
