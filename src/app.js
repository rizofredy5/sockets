import express from "express";
import path from "path";
import cors from "cors";
import morgan from "morgan";

import userRouter from "./router/user.routes";

const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan("dev"));

app.use(express.static(path.join(__dirname, "public")));

app.use("/api/users", userRouter);

export default app;
